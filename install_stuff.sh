opam switch 4.09.0
opam install mirage mirage-unix mirage-bootvar-unix mirage-net-unix mirage-clock-unix mirage-types-lwt mirage-entropy nocrypto tcpip crunch cohttp-mirage mirage-logs mirage-types
opam install irmin-unix irmin-fs irmin-git irmin-graphql irmin-http irmin-mem irmin-mirage irmin-mirage-git irmin-mirage-graphql irmin-unix irmin-pack
mirage configure -t unix
make depends
make
