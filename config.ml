open Mirage

type shellconfig = ShellConfig
let shellconfig = Type ShellConfig

let config_shell = impl @@ object
    inherit base_configurable

    method! build _i =
      Bos.OS.Cmd.run Bos.Cmd.(v "dd" % "if=/dev/zero" % "of=disk.img" % "count=100000")

    method! clean _i =
      Bos.OS.File.delete (Fpath.v "disk.img")

    method module_name = "Functoria_runtime"
    method name = "shell_config"
    method ty = shellconfig
end

let key =
  let doc = Key.Arg.info ~doc:"How to say hello." ["hello"] in
  Key.(create "hello" Arg.(opt string "Hello World!" doc))


let disk = generic_kv_ro "t"

let main =
    let packages = [ package "io-page"; package "duration"; package ~build:true "bos"; package ~build:true "fpath" ] in
  foreign
    ~packages
    ~keys:[Key.abstract key]
    ~deps:[abstract config_shell] "Unikernel.Main" (time @-> block  @-> console @-> network @-> ethernet @-> ipv6 @-> stackv4 @-> job)

let img = Key.(if_impl is_solo5 (block_of_file "storage") (block_of_file "disk.img"))

let net = default_network
let ethif = etif net

let ipv6 =
  let config = {
    addresses = [Ipaddr.V6.of_string_exn "ff23:db8::ff00:42:8329"];
    netmasks  = [Ipaddr.V6.Prefix.of_string_exn "ff20::/12"];
    gateways  = [Ipaddr.V6.of_string_exn "ff23:db8::ff00:42:8321"];
  } in
  create_ipv6 ethif config

(*
let ipv4cfg =
  let config = {
    ip = [Ipaddr.V4.of_string_exn "10.1.2.2"];
    network  = [Ipaddr.V4.Prefix.of_string_exn "10.1.2/24"]; 
    gateway  = [Ipaddr.V4.of_string_exn "10.1.2.1"];

  } in
  create_ipv4 ethif config 
*)

let stack = static_ipv4_stack default_network

let () =
  register "ping" [ main $ default_time $ img $ default_console $ default_network $ ethif $ ipv6 $ stack ]
